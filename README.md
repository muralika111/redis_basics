Introduction to Redis (Remote Dictionary Server)
----------------------------
* Redis is a in memory database, so its fast
* Redis is a nosql database
* Uses datastructure to strore data.
1) Redis supports strings, lists, sets, sorted sets, hashes(key value pairs) and also high level datatypes like      bitmaps, hyperlogs, geospatial indexess.
2) Redis is written in c language
  can perform  110,000 set s per second and about  81,000 get s per second
  
Installation :
------
* Install redis server : `sudo apt-get install redis-server`.
* Install redis-py : `sudo pip install redis`.
* Start a redis server: `redis-server`.
* Check if server is running: `redis-cli`, `ping` .
* To run redis from remote server : `redis-cli -h host -port password`

Common redis-cli commands :
-------
*  `publish` and  `subscribe`
* `set key value`
* `exists key`
* `expire key 50`
* `ttl key   #gives amount of time left`
* `persist key`
* `setex key value`
* `append key value`
* `mset key1 value1 key2 value2`
* `get key`
* `del key`
* `incr key`
* `incrby key value`
* `decr key`
* `flushall`

sets
----------
*  `sadd set_name "this" "is" "an" "example"`
*  `sismember set_name "is"`
* `smembers set_name`
* `sunion set1 set2`
* `sinter set1 set2s`

lists
----------
*  `lpush list_name 1 2 3 4`
*  `lrange list_name 0 10`
*  `lpop list_name`
*  `blpop list_name 100s`
*  `rpush list_name 'test'`

redis-py
-----------
*In order to use Redis with Python you will need a Python Redis client.*

 ```
 import redis
 r = redis.Redis(
    host='hostname',
    port=port
    )

#hostname ='localhost'
#port ='6379'
```
    

* Now `r` can be used as object on which redis methods can be applied
* Few examples are shown below

```
print(r.set("hello", "world") `

print(r.set('hello', 'world'))

print(r.get('hello'))

print(r.delete('hello'))
```

Pipeline function
--------
* Pipelines are a subclass of the base Redis class that provide support for buffering multiple commands to the server in a single request.

 ```
r = redis.Redis(...)
>>> r.set('bing', 'baz')
>>> # Use the pipeline() method to create a pipeline instance
>>> pipe = r.pipeline()
>>> # The following SET commands are buffered
>>> pipe.set('foo', 'bar')
>>> pipe.get('bing')
>>> # the EXECUTE call sends all buffered commands to the server, returning
>>> # a list of responses, one for each command.
>>> pipe.execute()
[True, 'baz']
```

* Also we can chain the set of commands

```
>>> pipe.set('foo', 'bar').sadd('faz', 'baz').incr('auto_number').execute()
[True, True, 6]
```

